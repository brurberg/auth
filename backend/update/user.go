package update

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.brurberg.no/sindre/brurbergauth/helper"
)

type DB struct {
	DB *helper.DB
}

func (db DB) UpdateUserEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return
	}
	var user helper.User
	err = json.Unmarshal(body, &user)
	if helper.CheckErrCode(c, err, http.StatusBadRequest) {
		return
	}
	err = db.DB.UpdateUser(user)
	if err != nil {
		if strings.HasPrefix(err.Error(), "Validate userinfo:") {
			helper.CheckErrCode(c, err, http.StatusBadRequest)
		} else {
			helper.CheckErr(c, err)
		}
	}
}

func (db DB) ClaimsEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	bearerToken, _ := helper.GetBearerToken(c.Request.Header.Get("authorization"))
	claims, _ := helper.VerifyJwt(bearerToken, db.DB.GetJWTSecret(c.Request.Host))
	userinfo := db.DB.GetUserInfoForSystem(c.Request.Host, claims["username"].(string))

	c.JSON(http.StatusOK, gin.H{"userinfo": userinfo})
}

type system struct {
	Host string `json:"host"`
}
