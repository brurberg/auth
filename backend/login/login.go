package login

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.brurberg.no/sindre/brurbergauth/helper"
)

type DB struct {
	DB *helper.DB
}

func (db DB) CreateTokenEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if helper.CheckErr(c, err) {
		return
	}
	user := make(map[string]interface{})
	err = json.Unmarshal(body, &user)
	if helper.CheckErrCode(c, err, http.StatusBadRequest) {
		return
	}
	ok, err := db.DB.CheckPassword([]byte(user["password"].(string)), user["username"].(string))
	if err != nil {
		helper.CheckErr(c, err)
		return
	} else if !ok {
		helper.CheckErrCode(c, fmt.Errorf("Wrong password"), http.StatusForbidden)
		return
	}
	user["authorized"] = false
	tokenString, err := helper.SignJwt(user, db.DB.GetJWTSecret(c.Request.Host))
	if helper.CheckErr(c, err) {
		return
	}
	json.NewEncoder(c.Writer).Encode(helper.JwtToken{Token: tokenString})
}
