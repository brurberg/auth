package main

import (
	"encoding/json"

	"github.com/dgryski/dgoogauth"
	"github.com/gin-gonic/gin"
	"gitlab.brurberg.no/sindre/brurbergauth/helper"
)

func (db DB) AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		bearerToken, err := helper.GetBearerToken(c.Request.Header.Get("authorization"))
		if err != nil {
			json.NewEncoder(c.Writer).Encode(err)
			c.Abort()
			return
		}
		_, err = helper.VerifyJwt(bearerToken, db.DB.GetJWTSecret(c.Request.Host))
		if err != nil {
			json.NewEncoder(c.Writer).Encode(err)
			c.Abort()
			return
		}
		c.Next()
	}
}

func (db DB) VerifyOtpEndpoint(c *gin.Context) {
	bearerToken, err := helper.GetBearerToken(c.Request.Header.Get("authorization"))
	if err != nil {
		json.NewEncoder(c.Writer).Encode(err)
		return
	}
	jwtSecret := db.DB.GetJWTSecret(c.Request.Host)
	decodedToken, err := helper.VerifyJwt(bearerToken, jwtSecret)
	if err != nil {
		json.NewEncoder(c.Writer).Encode(err)
		return
	}
	otpc := &dgoogauth.OTPConfig{
		Secret:      db.DB.GetOTPSecret(decodedToken["username"].(string)),
		WindowSize:  3,
		HotpCounter: 0,
	}
	var otpToken helper.OtpToken
	_ = json.NewDecoder(c.Request.Body).Decode(&otpToken)
	decodedToken["authorized"], _ = otpc.Authenticate(otpToken.Token)
	if decodedToken["authorized"] == false {
		json.NewEncoder(c.Writer).Encode("Invalid one-time password")
		return
	}
	jwToken, _ := helper.SignJwt(decodedToken, jwtSecret)
	json.NewEncoder(c.Writer).Encode(jwToken)
}
