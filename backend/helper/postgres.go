package helper

import (
	"crypto/rand"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"regexp"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"golang.org/x/crypto/scrypt"
)

type DBConnect struct {
	DB_HOST     string
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string
}

type DB struct {
	Conn *sql.DB
}

type JSONTime time.Time

func (t JSONTime) MarshalJSON() ([]byte, error) {
	//do your serializing here
	stamp := fmt.Sprintf("\"%s\"", time.Time(t).Format("06.01.02"))
	return []byte(stamp), nil
}
func (t JSONTime) UnmarshalJSON(data []byte) error {
	log.Println(string(data[1 : len(data)-1]))
	j, err := time.Parse("06/01/02", string(data[1:len(data)-1]))
	log.Println(j)
	return err
}

type User struct {
	Username   string `json:"username"`
	Password   string `json:"password"`
	Firstname  string `json:"firstname"`
	Sirname    string `json:"sirname"`
	Email      string `json:"email"`
	Birthday   string `json:"birthday"`
	Address    string `json:"address"`
	Postnumber string `json:"postnumber"`
}

func (dbc DBConnect) ConnectToDB() (*sql.DB, error) {
	dbinfo := ""
	if dbc.DB_PASSWORD == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
	} else {
		dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
	}

	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return nil, err
	}

	return db, db.Ping()
}

func CheckErr(c *gin.Context, err error) bool {
	return CheckErrCode(c, err, http.StatusInternalServerError)
}

func CheckErrCode(c *gin.Context, err error, errorCode int) bool {
	if err != nil {
		if errorCode == 0 {
			errorCode = http.StatusInternalServerError
		}
		c.JSON(errorCode, gin.H{"error": err.Error()})
		return true
	} else {
		return false
	}
}

func CheckRowAff(c *gin.Context, result sql.Result) bool {
	rowAff, err := result.RowsAffected()
	if rowAff < 1 {
		c.JSON(http.StatusInternalServerError, err.Error())
		return true
	}
	return false
}

type Rights struct {
	FirstName bool `json:"firstName"`
	LastName  bool `json:"lastName"`
	Email     bool `json:"email"`
	AgeRange  bool `json:"ageRange"`
	Birthdate bool `json:"birthdate"`
	Address   bool `json:"address"`
	Region    bool `json:"region"`
	Country   bool `json:"country"`
}

type System struct {
	Host   string `json:"host"`
	Rights Rights `json:"Rights"`
}

func (db DB) RegisterSystem(system System) (err error) {
	result, err := db.Conn.Exec("INSERT INTO systems (host, JWTSecret, FirstName, LastName, Email, AgeRange, Birthdate, Address, Region, Country) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);", system.Host, GenRandBase32String(64), system.Rights.FirstName, system.Rights.LastName, system.Rights.Email, system.Rights.AgeRange, system.Rights.Birthdate, system.Rights.Address, system.Rights.Region, system.Rights.Country)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}

func (db DB) RegisterSystemConsent(rights Rights, user, host string) (err error) {
	result, err := db.Conn.Exec("INSERT INTO systemconsent (host, username, FirstName, LastName, Email, AgeRange, Birthdate, Address, Region, Country) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);", host, user, rights.FirstName, rights.LastName, rights.Email, rights.AgeRange, rights.Birthdate, rights.Address, rights.Region, rights.Country)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}

func (db DB) GetUserInfoForSystem(host, user string) (userinfo map[string]string) {
	var userConsents []bool
	db.Conn.QueryRow("SELECT FirstName, LastName, Email, AgeRange, Birthdate, Address, Region, Country FROM systemconsent WHERE host = $1 AND username = $2;", host, user).Scan(&userConsents)
	stmt := "SELECT"
	lenBase := len(stmt)
	if userConsents[0] {
		stmt += " FirstName"
	}
	if userConsents[1] {
		needsComma(&stmt, lenBase)
		stmt += " LastName"
	}
	if userConsents[2] {
		needsComma(&stmt, lenBase)
		stmt += " Email"
	}
	if userConsents[3] {
		needsComma(&stmt, lenBase)
		stmt += " AgeRange"
	}
	if userConsents[4] {
		needsComma(&stmt, lenBase)
		stmt += " Birthdate"
	}
	if userConsents[5] {
		needsComma(&stmt, lenBase)
		stmt += " Address"
	}
	if userConsents[6] {
		needsComma(&stmt, lenBase)
		stmt += " Region"
	}
	if userConsents[7] {
		needsComma(&stmt, lenBase)
		stmt += " Country"
	}
	if len(stmt) == lenBase {
		return
	}
	stmt += " FROM userdata WHERE username = $1;"
	db.Conn.QueryRow(stmt, user).Scan(&userinfo)
	return
}

func needsComma(stmt *string, lim int) {
	if len(*stmt) > 5 {
		*stmt = *stmt + ","
	}
}

func (db DB) GetJWTSecret(host string) (secret []byte) {
	db.Conn.QueryRow("SELECT JWTSecret FROM systems WHERE host = $1;", host).Scan(&secret)
	return
}

func (db DB) GetOTPSecret(username string) (secret string) {
	db.Conn.QueryRow("SELECT MFASecret FROM userdata WHERE username = $1;", username).Scan(&secret)
	return
}

const (
	PW_SALT_BYTES = 32
	PW_HASH_BYTES = 64
)

func (db DB) getSalt(username string) (salt []byte) {
	db.Conn.QueryRow("SELECT salt FROM userdata WHERE username = $1;", username).Scan(&salt)
	return
}

func (db DB) getPasswordHash(username string) (hash []byte) {
	db.Conn.QueryRow("SELECT passwordHash FROM userdata WHERE username = $1;", username).Scan(&hash)
	return
}

func HashPassword(password, salt []byte) ([]byte, error) {
	if len(salt) < PW_SALT_BYTES {
		return nil, errors.New("Too week salt")
	}
	hash, err := scrypt.Key(password, salt, 1<<14, 8, 1, PW_HASH_BYTES)
	return hash, err
}

func validateUserInfo(user User) (birthday time.Time, err error) {
	if len(user.Birthday) > 0 {
		birthday, err = time.Parse("06/01/02", user.Birthday)
		if err != nil {
			return
		}
		now := time.Now()
		threshold := time.Date(now.Year()-6, now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
		if birthday.Before(threshold) {
			err = fmt.Errorf("You need to be atleast 6 years to register")
			return
		}
	}
	if len(user.Password) < 8 {
		err = fmt.Errorf("Password needs to be atleast 8 charecters.")
		return
	}
	var number = regexp.MustCompile(`[0-9]`)
	var lower = regexp.MustCompile(`[a-z]`)
	var upper = regexp.MustCompile(`[A-Z]`)
	var notCharOrNumber = regexp.MustCompile(`[^a-zA-Z0-9 ,._-]`)
	if !(number.MatchString(user.Password) && lower.MatchString(user.Password) && upper.MatchString(user.Password)) {
		err = fmt.Errorf("Password does not meet requrements of number, lower and upercase char.")
		return
	}
	if len(user.Firstname) > 2 || number.MatchString(user.Firstname) {
		err = fmt.Errorf("Firstname needs to be longer then 2 char and not contain numbers")
		return
	}
	if len(user.Sirname) > 2 || number.MatchString(user.Sirname) {
		err = fmt.Errorf("Sirname needs to be longer then 2 char and not contain numbers")
		return
	}
	var email = regexp.MustCompile(`^[a-zA-Z0-9.!#$%&'*+/=?^_{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$`)
	if len(user.Email) > 0 && !email.MatchString(user.Email) {
		err = fmt.Errorf("Your email does not match email format")
		return
	}
	if len(user.Address) > 0 && notCharOrNumber.MatchString(user.Address) {
		err = fmt.Errorf("Address can only contain charecters, numbers and spaces")
		return
	}
	if len(user.Postnumber) > 0 && notCharOrNumber.MatchString(user.Postnumber) {
		err = fmt.Errorf("Postnumber can only contain charecters, numbers, spaces and ,._-")
		return
	}
	return
}

func (db DB) RegisterUser(user User, consent string, mailInstedOfMFA bool) (otpSecret, emailVerificationCode string, err error) {
	if consent == "" {
		err = fmt.Errorf("Consent for GDPR form is needed to store userdata and therfore creating a user.")
		return
	}

	if mailInstedOfMFA && len(user.Email) == 0 {
		err = fmt.Errorf("Email was not provided when verifying with mail insted of MFA")
		return
	}
	birthday, err := validateUserInfo(user)
	if err != nil {
		err = fmt.Errorf("Validate userinfo: %v", err)
		return
	}

	salt := make([]byte, PW_SALT_BYTES)
	_, err = io.ReadFull(rand.Reader, salt)
	if err != nil {
		log.Fatal(err)
	}
	hash, err := HashPassword([]byte(user.Password), salt)
	if !mailInstedOfMFA {
		otpSecret = GenerateOTPSecret()
	}
	if len(user.Email) != 0 {
		emailVerificationCode = GenRandBase32String(6)
	} //TODO: add this to db and verify

	result, err := db.Conn.Exec("INSERT INTO userdata (username, salt, passwordHash, MFASecret, Firstname, Sirname, Email, EmailVerification, Birthday, Address, Postnumber) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11);", user.Username, string(salt), string(hash), otpSecret, user.Firstname, user.Sirname, user.Email, emailVerificationCode, birthday, user.Address, user.Postnumber)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error, less then one row affected")
		}
		return
	}
	return
}

func (db DB) VerifyEmail(username string) (emailVCode string) {
	db.Conn.QueryRow("SELECT EmailVerification FROM userdata WHERE username = $1;", username).Scan(&emailVCode)
	return
}

func (db DB) UpdateUser(user User) (err error) {
	birthday, err := validateUserInfo(user)
	if err != nil {
		err = fmt.Errorf("Validate userinfo: %v", err)
		return
	}

	query := "UPDATE userdata SET Firstname = $1, Sirname = $2, Email = $3, Birthday = $4, Address = $5, Postnumber = $6 WHERE username = $7"
	result, err := db.Conn.Exec(query, user.Firstname, user.Sirname, user.Email, birthday, user.Address, user.Postnumber, user.Username)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error, less then one row affected")
		}
		return
	}
	return
}

func checkHashes(a, b []byte) bool {
	if (a == nil) || (b == nil) {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}

func (db DB) CheckPassword(password []byte, username string) (bool, error) {
	hash, err := HashPassword(password, db.getSalt(username))
	if err != nil {
		return false, err
	}

	return checkHashes(hash, db.getPasswordHash(username)), nil
}
