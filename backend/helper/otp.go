package helper

import (
	"crypto/rand"
	"encoding/base32"
)

type OtpToken struct {
	Token string `json:"otp"`
}

func GenRandBase32String(length int) string {
	random := make([]byte, length)
	rand.Read(random)
	return base32.StdEncoding.EncodeToString(random)
}

func GenerateOTPSecret() string {
	return GenRandBase32String(10)
}
