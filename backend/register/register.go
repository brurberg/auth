package register

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.brurberg.no/sindre/brurbergauth/helper"
)

type DB struct {
	DB *helper.DB
}

type registerUser struct {
	User            helper.User `json:"user"`
	Consent         string      `json:"consent"`
	MailInstedOfMFA bool        `json:"mailInstedOfMFA"`
}

func (db DB) RegisterUserEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return
	}
	var user registerUser
	err = json.Unmarshal(body, &user)
	if err != nil {
		log.Println(err)
		return
	}
	otpSecret, emailVerificationCode, err := db.DB.RegisterUser(user.User, user.Consent, user.MailInstedOfMFA)
	if err != nil {
		log.Println(err)
		return
	}
	if err = SendValidationEmail(user.User.Email, emailVerificationCode); err != nil {
		log.Println(err)
	}
	json.NewEncoder(c.Writer).Encode(otpSecret)
}

type verifyMail struct {
	EmailVCode string `json:"emailvcode"`
}

func (db DB) VerifyEmailEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if helper.CheckErr(c, err) {
		return
	}
	bearerToken, _ := helper.GetBearerToken(c.Request.Header.Get("authorization"))
	claims, _ := helper.VerifyJwt(bearerToken, db.DB.GetJWTSecret(c.Request.Host))
	var verify verifyMail
	err = json.Unmarshal(body, &verify)
	if helper.CheckErrCode(c, err, http.StatusBadRequest) {
		return
	}
	emailVCode := db.DB.VerifyEmail(claims["username"].(string))
	if emailVCode != verify.EmailVCode {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Verification code was not correct", "verified": false})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Success, email verified", "verified": true})
}

func (db DB) RegisterSystemEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if helper.CheckErr(c, err) {
		return
	}
	var sys helper.System
	err = json.Unmarshal(body, &sys)
	if helper.CheckErrCode(c, err, http.StatusBadRequest) {
		return
	}
	err = db.DB.RegisterSystem(sys)
	if helper.CheckErr(c, err) {
		return
	}
	//json.NewEncoder(c.Writer).Encode(otpSecret)
}

func (db DB) RegisterSystemConsentEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if helper.CheckErr(c, err) {
		return
	}
	bearerToken, _ := helper.GetBearerToken(c.Request.Header.Get("authorization"))
	claims, _ := helper.VerifyJwt(bearerToken, db.DB.GetJWTSecret(c.Request.Host))
	var consent helper.Rights
	err = json.Unmarshal(body, &consent)
	if helper.CheckErrCode(c, err, http.StatusBadRequest) {
		return
	}
	err = db.DB.RegisterSystemConsent(consent, claims["username"].(string), c.Request.Host)
	if helper.CheckErr(c, err) {
		return
	}
	//json.NewEncoder(c.Writer).Encode(otpSecret)
}
