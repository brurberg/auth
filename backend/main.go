package main

import (
	"log"
	"runtime"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.brurberg.no/sindre/brurbergauth/helper"
	"gitlab.brurberg.no/sindre/brurbergauth/login"
	"gitlab.brurberg.no/sindre/brurbergauth/register"
	"gitlab.brurberg.no/sindre/brurbergauth/update"
)

func loadEnvVar() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
}

type DB struct {
	DB *helper.DB
}

func main() {
	loadEnvVar()

	r := gin.Default()
	dbCon, dbErr := helper.DBConnect{DB_HOST: "172.17.0.2", DB_USER: "brurbergauth", DB_PASSWORD: "N2RIGTVGWSXDF5N4", DB_NAME: "brurbergauth"}.ConnectToDB()
	if dbErr != nil {
		log.Fatal(dbErr)
	}
	db := DB{DB: &helper.DB{Conn: dbCon}}
	login := login.DB{DB: db.DB}
	reg := register.DB{DB: db.DB}
	upd := update.DB{DB: db.DB}

	api := r.Group("/api")
	api.POST("/registeruser", reg.RegisterUserEndpoint)
	api.POST("/authenticate", login.CreateTokenEndpoint)

	auth := api.Group("/")
	auth.Use(db.AuthMiddleware())
	auth.POST("/verify-otp", db.VerifyOtpEndpoint)

	mfa := api.Group("/")
	mfa.Use(db.MFAMiddleware())
	mfa.POST("/registersystem", reg.RegisterSystemEndpoint) //TODO: change back to mfa not api
	mfa.POST("/registersystemconsent", reg.RegisterSystemConsentEndpoint)
	mfa.POST("/updateclaims", upd.ClaimsEndpoint)
	mfa.POST("/updateuserinfo", upd.UpdateUserEndpoint)
	mfa.POST("/verifyemail", reg.VerifyEmailEndpoint)
	mfa.GET("/specs", func(c *gin.Context) {
		c.JSON(200, gin.H{"OS": runtime.GOOS, "Threads": runtime.NumCPU()})
	})

	r.Run(":3000")
}
