package main

import (
	"encoding/json"

	"github.com/gin-gonic/gin"
	"gitlab.brurberg.no/sindre/brurbergauth/helper"
)

func (db DB) MFAMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		bearerToken, err := helper.GetBearerToken(c.Request.Header.Get("authorization"))
		if err != nil {
			json.NewEncoder(c.Writer).Encode(err)
			c.Abort()
			return
		}
		decodedToken, err := helper.VerifyJwt(bearerToken, db.DB.GetJWTSecret(c.Request.Host))
		if err != nil {
			json.NewEncoder(c.Writer).Encode(err)
			c.Abort()
			return
		}
		if decodedToken["authorized"] != true {
			json.NewEncoder(c.Writer).Encode("2FA is required")
			c.Abort()
			return
		}
		c.Next()
	}
}
