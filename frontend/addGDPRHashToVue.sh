git log --output=id.txt -n 1 --pretty=format:%H -- src/assets/GDPRConsentForm.md | echo
printf "const GDPRConsentFormCommitID = \"" >> ./src/constants.js
cat id.txt >> ./src/constants.js
printf "\"\n" >> ./src/constants.js
printf "module.exports = {GDPRConsentFormCommitID}\n" >> ./src/constants.js
