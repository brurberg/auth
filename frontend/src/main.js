import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'
import vuetify from './plugins/vuetify';
import VueShowdown from 'vue-showdown'

Vue.use(VueResource)
Vue.use(Vuetify, {
  theme: {
    secondary: colors.orange
  }
})
Vue.use(VueShowdown, {
  options: {
    emoji: true
  }
})

Vue.config.productionTip = false

const token = localStorage.getItem('token')
if (token) {
  Vue.http.interceptors.push(function(request) {
    request.headers.set('Authorization', "Bearer " + token)
  })
}

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
