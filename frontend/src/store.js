import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    claims : parseJwt(localStorage.getItem('token') || ''),
    mailInsteadOfMFA : false,
    otpsecret : ''
  },
  mutations: {
    auth_request(state){
      state.status = 'loading'
    },
    auth_success(state, payload){
      state.status = 'success'
      state.token = payload[0]
      state.claims = parseJwt(payload[0])
      state.otpsecret = payload[1]
    },
    auth_error(state){
      state.status = 'error'
    },
    logout(state){
      state.status = ''
      state.token = ''
      state.claims = {}
      state.otpsecret = ''
    },
    mailInsteadOfMFA(state, payload) {
      state.mailInsteadOfMFA = payload[0]
    }
  },
  actions: {
    authenticate({commit}, user){
      return new Promise((resolve, reject) => {
        commit('auth_request')
        Vue.http({url: '/api/authenticate', body: user, method: 'POST' })
        .then(resp => {
          const token = resp.data.token
          localStorage.setItem('token', token)
          Vue.http.interceptors.push(function(request) {
            request.headers.set('Authorization', "Bearer " + token)
          })
          commit('auth_success', [token, ''])
          resolve(resp)
        })
        .catch(err => {
          commit('auth_error')
          localStorage.removeItem('token')
          reject(err)
        })
      })
    },
    verifyotp({commit}, otp){
      return new Promise((resolve, reject) => {
        commit('auth_request')
        Vue.http({url: '/api/verify-otp', body: otp, method: 'POST' })
        .then(resp => {
          const token = resp.data.token
          localStorage.setItem('token', token)
          Vue.http.interceptors.push(function(request) {
            request.headers.set('Authorization', "Bearer " + token)
          })
          commit('auth_success', [token, ''])
          resolve(resp)
        })
        .catch(err => {
          commit('auth_error')
          localStorage.removeItem('token')
          reject(err)
        })
      })
    },
    registeruser({commit}, user){
      return new Promise((resolve, reject) => {
        commit('auth_request')
        commit('mailInsteadOfMFA', [user.mailInsteadOfMFA])
        Vue.http({url: '/api/registeruser', body: user, method: 'POST' })
        .then(resp => {
          const token = resp.data.token
          const otpsecret = resp.data.otpkey
          localStorage.setItem('token', token)
          Vue.http.interceptors.push(function(request) {
            request.headers.set('Authorization', "Bearer " + token)
          })
          commit('auth_success', [token, otpsecret])
          resolve(resp)
        })
        .catch(err => {
          commit('auth_error', err)
          localStorage.removeItem('token')
          reject(err)
        })
      })
    },
    logout({commit}){
      return new Promise((resolve) => {
        commit('logout')
        localStorage.removeItem('token')
        localStorage.removeItem('claims')
        delete Vue.http.defaults.headers.common['Authorization']
        resolve()
      })
    }
  }
})

function parseJwt (token) {
  if (!token || token == undefined) {
    return {}
  }
  var base64Url = token.split('.')[1]
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
  var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
  }).join(''))

  return JSON.parse(jsonPayload)
}
